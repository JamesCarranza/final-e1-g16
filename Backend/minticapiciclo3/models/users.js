const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const usersSchema = new Schema({
    nombre: {type: String, required: [true, 'Nombre obligatorio']},
    apellido: {type: String, required: [true, 'Apellido obligatorio']},
    email: {type: String, required: [true, 'Email obligatorio']},
    tipo_doc: {type: String, required: [true, 'Tipo Documento obligatorio']},
    doc: {type: String, required: [true, 'Documento obligatorio']},
    pass: {type: String, required: [true, 'Password obligatorio']},
    date:{type: Date, default: Date.now},
    activo: {type: Boolean, default: true}
});

// Convertir a modelo
const Users = mongoose.model('users', usersSchema);

module.exports = Users;

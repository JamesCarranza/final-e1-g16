const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const eventsSchema = new Schema({
    nombre: {type: String, required: [true, 'Nombre Evento obligatorio']},
    fecha: {type: Date,Default: Date.now, required: [true, 'Fecha Evento obligatoria']},
    descripcion: {type: String, required: [true, 'Descripcion Evento obligatoria']},
    urlImagen: {type: String, required: [true, 'URL Imagen obligatoria']},
    activo: {type: Boolean, default: true}
});

// Convertir a modelo
const Events = mongoose.model('events', eventsSchema);

module.exports = Events;

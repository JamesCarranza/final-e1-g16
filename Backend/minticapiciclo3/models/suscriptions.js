const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const suscriptionsSchema = new Schema({
    idEvent: {type: String, required: [true, 'Id Evento obligatorio']},
    idUser: {type: String, required: [true, 'Id usuario obligatorio']},
    date: {type: Date,Default: Date.now},
});

// Convertir a modelo
const Suscriptions = mongoose.model('suscriptions', suscriptionsSchema);

module.exports = Suscriptions;

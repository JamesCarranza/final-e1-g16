const express = require('express');
const morgan = require('morgan');
const cors = require('cors');


const app = express();

// Middleware
app.use(morgan('tiny'));
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
// app.use(express.static(path.join(__dirname, 'public')));

// Rutas
app.get('/', (req, res) => {
    res.send('Hola Mundo');
});

// usuarios
app.use('/api', require('./routes/user'));

// Eventos
app.use('/api', require('./routes/events'));

// Suscribirse
app.use('/api', require('./routes/suscriptions'));




app.set('puerto', process.env.PORT || 3000);
app.listen(app.get('puerto'), () => {
    console.log('Example app listening on port'+ app.get('puerto'));
});

// Conexión base de datos
const mongoose = require('mongoose');

const uri = 'mongodb+srv://ebs16:ebs16@cluster0.xbbcs.mongodb.net/ebs?retryWrites=true&w=majority';
const options = {useNewUrlParser: true};

// Or using promises
mongoose.connect(uri, options).then(
    /** ready to use. The `mongoose.connect()` promise resolves to mongoose instance. */
    () => { console.log('Conectado a DB') },
    /** handle initial connection error */
    err => { console.log(err) }
);

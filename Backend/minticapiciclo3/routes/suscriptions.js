const express = require('express');
const router = express.Router();

// importar el modelo Evento
const Suscriptions = require('../models/suscriptions');

// Agregar una suscripcion
router.post('/suscription/nuevo', async(req, res) => {
    const body = req.body;
    try {
        const suscriptionDB = await Suscriptions.create(body);
        res.status(200).json(suscriptionDB);
    } catch (error) {

        return res.status(500).json({
            mensaje: error,
            error
        })
    }
});


// Exportamos la configuración de express app
module.exports = router;

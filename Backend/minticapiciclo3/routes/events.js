const express = require('express');
const router = express.Router();

// importar el modelo Evento
const Events = require('../models/events');

// Agregar un evento
router.post('/event/nuevo', async(req, res) => {
    const body = req.body;
    try {
        const eventDB = await Events.create(body);
        res.status(200).json(eventDB);
    } catch (error) {

        return res.status(500).json({
            mensaje: error,
            error
        })
    }
});

// Obtener un evento

router.get('/event/:id', async(req, res) => {
    const _id = req.params.id;
    try {
        const eventDB = await Events.findOne({_id});
        res.json(eventDB);
    } catch (error) {
        return res.status(400).json({
            mensaje: 'Ocurrio un error',
            error
        })
    }
});

// Obtener todos los eventos

router.get('/event/get/all', async(req, res) => {
    try {
        const eventDB = await Events.find({});
        res.json(eventDB);
    } catch (error) {
        return res.status(400).json({
            mensaje: 'Ocurrio un error',
            error
        })
    }
});


// Actualizar un evento

router.put('/event/:id', async(req, res) => {
    const _id = req.params.id;
    const body = req.body;
    try {
        const eventDB = await Events.findByIdAndUpdate(
            _id,
            body,
            {new: true});
        res.json(eventDB);
    } catch (error) {
        return res.status(400).json({
            mensaje: 'Ocurrio un error',
            error
        })
    }
});


// Exportamos la configuración de express app
module.exports = router;

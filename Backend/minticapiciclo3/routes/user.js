const express = require('express');
const router = express.Router();

// importar el modelo usuario
const Users = require('../models/users');

// Agregar una usuario
router.post('/user/nuevo', async(req, res) => {
    const body = req.body;
    try {
        const userDB = await Users.create(body);
        res.status(200).json(userDB);
    } catch (error) {

        return res.status(500).json({
            mensaje: error,
            error
        })
    }
});

// Obtener un usuario

router.get('/user/:id', async(req, res) => {
    const _id = req.params.id;
    try {
        const userDB = await Users.findOne({_id});
        res.json(userDB);
    } catch (error) {
        return res.status(400).json({
            mensaje: 'Ocurrio un error',
            error
        })
    }
});

router.get('/user/email/:email', async(req, res) => {
    const email = req.params.email;
    try {
        const userDB = await Users.findOne({email});
        res.json(userDB);
    } catch (error) {
        return res.status(400).json({
            mensaje: 'Ocurrio un error',
            error
        })
    }
});

// Actualizar un usuario

router.put('/user/:id', async(req, res) => {
    const _id = req.params.id;
    const body = req.body;
    try {
        const userDb = await Users.findByIdAndUpdate(
            _id,
            body,
            {new: true});
        res.json(userDb);
    } catch (error) {
        return res.status(400).json({
            mensaje: 'Ocurrio un error',
            error
        })
    }
});


// Exportamos la configuración de express app
module.exports = router;

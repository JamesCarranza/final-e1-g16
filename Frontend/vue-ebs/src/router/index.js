import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import Perfil from '../views/Perfil.vue'
import Login from '../views/Login.vue'
import Registro from '../views/Registro.vue'
Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/perfil',
    name: 'Perfil',
    component: Perfil
  }
  ,
  {
    path: '/login',
    name: 'Login',
    component: Login
  }
  ,
  {
    path: '/registro',
    name: 'Registro',
    component: Registro
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
